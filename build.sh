#!/bin/bash
HERE=$(pwd)

yarn install -s

for dir in modules/*; do
    if [[ -d "$dir" && ! -L "$dir" ]]; then
        mkdir -p tmp/${dir#*/}/node_modules
        cp -r node_modules tmp/${dir#*/}
        cp -r libs tmp/${dir#*/}/node_modules/
        cp -r $dir/* tmp/${dir#*/}/
        cd tmp/${dir#*/}
        zip -FSrq ../../builds/${dir#*/}.zip *
        cd $HERE
    fi
done

rm -r tmp