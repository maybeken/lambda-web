const MODULE_NAME = "Logging"

module.exports = ({ level, moduleName, message }) => {
    console.log(`[${level}] ${moduleName}: ${message}`)
}