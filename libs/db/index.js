const MODULE_NAME = "Database"

const libs = require("libs")("log")

const AWS = require('aws-sdk')
AWS.config.update({ region: process.env.AWS_REGION })

const dynamodb = new AWS.DynamoDB({ apiVersion: '2012-10-08' })

module.exports = {
    get: async ({ keys, tables }) => {
        let result = {}
        let RequestItems = {}

        for (let i in tables) {
            RequestItems[tables[i]] = {
                "Keys": keys
            }
        }

        try {
            result = await dynamodb.batchGetItem({ RequestItems }).promise()
        } catch (err) {
            libs.log({ level: "ERROR", message: err, moduleName: MODULE_NAME })
            return false
        }

        return result.Responses
    },
    put: async ({ table, item }) => {
        try {
            result = await dynamodb.putItem({ TableName: table, Item: item }).promise()
        } catch (err) {
            libs.log({ level: "ERROR", message: err, moduleName: MODULE_NAME })
            return false
        }

        return result
    },
    delete: async ({ table, key }) => {
        try {
            result = await dynamodb.deleteItem({ TableName: table, Key: key }).promise()
        } catch (err) {
            libs.log({ level: "ERROR", message: err, moduleName: MODULE_NAME })
            return false
        }

        return result
    }
}