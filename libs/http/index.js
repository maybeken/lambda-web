const MODULE_NAME = "HTTP"

module.exports = {
    res: ({ code = 500, message = null, head = null }) => {

        let statusCode = 500
        let body = ""
        let error = false
        let headers = {
            ...head,
            "Content-Type": "application/json"
        }

        if (typeof code !== "number") {
            error = "HTTP code must be a number"
        }

        if (error) {
            body = JSON.stringify({ error })
        } else if (message === null) {
            statusCode = code
            body = JSON.stringify({})
        } else if (typeof message === "string") {
            statusCode = code
            body = JSON.stringify({ message })
        } else {
            statusCode = code
            body = JSON.stringify(message)
        }

        return { statusCode, body, headers }
    },
    router: async ({ event = {}, routes = {} }) => {
        if (routes.hasOwnProperty(event.path) && routes[event.path].hasOwnProperty(event.httpMethod) && typeof routes[event.path][event.httpMethod] === "function") {
            return await routes[event.path][event.httpMethod]({
                body: event.body,
                queryString: event.queryStringParameters,
                httpMethod: event.httpMethod,
                path: event.path,
                user: event.requestContext.identity.user,
                userAgent: event.requestContext.identity.userAgent,
                sourceIp: event.requestContext.identity.sourceIp,
                headers: event.headers,
                requestId: event.requestContext.requestId
            })
        } else {
            return false
        }
    }
}