const MODULE_NAME = "Library"

module.exports = (...libs) => {
    let modules = {}

    for(let i in libs) {
        let lib = libs[i]
        modules[lib] = require(`./${lib}`)
    }

    return modules
}