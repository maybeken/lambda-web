const MODULE_NAME = "User"

const libs = require("libs")("log", "cfg", "http", "db")
const uuid = require("uuid/v4")

exports.handler = async (event) => {
    libs.log({ level: "DEBUG", message: "Testing", moduleName: MODULE_NAME })

    let routes = {
        "/user/login": {
            "POST": async ({ queryString, sourceIp }) => {
                if (queryString.username && queryString.password) {
                    // TODO: Automate DB datatype determind
                    let uuidDB = await libs.db.get({
                        keys: [
                            { "username": { S: queryString.username } }
                        ],
                        tables: [`${libs.cfg.dbPrefix}-user-username`]
                    })

                    if (uuidDB) {
                        let passwordDB = await libs.db.get({
                            keys: [
                                // TODO: Consolidate DB result
                                { "uuid": { S: uuidDB[`${libs.cfg.dbPrefix}-user-username`][0]["uuid"]["S"] } }
                            ],
                            tables: [`${libs.cfg.dbPrefix}-user-main`]
                        })

                        if (passwordDB) {
                            if (passwordDB[`${libs.cfg.dbPrefix}-user-main`][0]["password"]["S"] == queryString.password) {
                                let token = uuid()
                                // TODO: Put session expire time to config file
                                // TODO: Time human format library
                                let expire = Math.floor((Date.now() + 48 * 60 * 60) / 1000)

                                let item = {
                                    'token': { S: token },
                                    'user_uuid': { S: uuidDB[`${libs.cfg.dbPrefix}-user-username`][0]["uuid"]["S"] },
                                    'ip': { S: sourceIp },
                                    'expire': { N: String(expire) }
                                }

                                let sessionDB = await libs.db.put({ table: `${libs.cfg.dbPrefix}-user-sessions`, item })

                                return libs.http.res({ code: 200, message: { token, expire } })
                            } else {
                                return libs.http.res({ code: 403 })
                            }
                        } else {
                            return libs.http.res({ code: 500 })
                        }
                    } else {
                        return libs.http.res({ code: 500 })
                    }
                } else {
                    return libs.http.res({ code: 400 })
                }
            }
        },
        "/user/session": {
            "POST": async () => {
                // TODO: Session renewal
            }
        }
    }

    return await libs.http.router({ event, routes }) || libs.http.res({ code: 404 })
}